import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

TestData nLogin = findTestData('Data Files/nlogin')

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.website)

WebUI.verifyElementVisible(findTestObject('Homepage/txt_CURA Healthcare Service'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Homepage/btn_hamburger'))

WebUI.click(findTestObject('Homepage/btn_login page'))

for (int rowData = 1; rowData <= nLogin.getRowNumbers(); rowData++) {
    WebUI.setText(findTestObject('Login/input_Username'), nLogin.getValue(1, rowData), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('Login/input_Password'), nLogin.getValue(2, rowData), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('Login/btn_Login'))

    WebUI.verifyElementVisible(findTestObject('Login/txt_login failed'))

    WebUI.takeScreenshot()
}

WebUI.closeBrowser()

